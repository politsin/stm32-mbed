#include <mbed.h>

DigitalOut led(LED1);
Serial     usart2(PA_2, PA_3); // TX, RX

void callback() {
  usart2.printf("callback %c\n", usart2.getc());
}

int main() {
  printf("Hello World !\n");
  usart2.baud(115200);
  usart2.attach(&callback);
  while(1) {
    wait(1); // 1 second
    led = !led; // Toggle LED
    usart2.printf("Hello World -2\n");
  }
}
